import { BrowserRouter, Routes, Route } from 'react-router-dom'
import Navbar from './components/navbar'
import Signin from './pages/user/signin'
import Home from './pages/user/home'
import Signup from './pages/user/signup'
import Mycars from './pages/car/myCars'
import AddCar from './pages/car/addCar'
import UploadImage from './pages/car/uploadImage'

import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

function App() {
  return (
    <BrowserRouter>
      <Navbar />
      <Routes>
        <Route path='/home' element={<Home />} />
        <Route path='/signin' element={<Signin />} />
        <Route path='/signup' element={<Signup />} />
        <Route path='/myCars' element={<Mycars />} />
        <Route path='/addCar' element={<AddCar />} />
        <Route path='/upload-image' element={<UploadImage />} />
      </Routes>
      <ToastContainer position='top-center' autoClose={1000} />
    </BrowserRouter>
  )
}

export default App
