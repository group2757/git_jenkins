import image from '../../images/add.webp'
import { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import axios from 'axios'
import { toast } from 'react-toastify'

const AddCar = () => {
  const [name, setName] = useState('')
  const [model, setModel] = useState('')
  const [price, setPrice] = useState('')
  const [carColor, setCarColor] = useState('')

  const navigate = useNavigate()

  const addCar = () => {
    if (name.length === 0) {
      toast.error('please enter name')
    } else if (model.length === 0) {
      toast.error('please enter car model')
    } else if (price === 0) {
      toast.error('please enter price')
    } else if (carColor.length === 0) {
      toast.error('please enter car color')
    } else {
      axios
        .post(
          'http://localhost:4000/car/add',
          {
            name,
            model,
            price,
            carColor,
          },
          { headers: { token: sessionStorage['token'] } }
        )
        .then((response) => {
          const result = response.data
          if ((result['status'] = 'success')) {
            toast.success('car added successfully')
            navigate('/myCars')
          } else {
            toast.error(result['error'])
          }
        })
        .catch((error) => {
          console.log('error')
          console.log(error)
        })
    }
  }

  return (
    <div style={styles.background}>
      <div style={styles.container}>
        <div>
          <h3
            style={{
              textAlign: 'center',
              marginBottom: 10,
              color: 'White',
            }}>
            ADD CAR
          </h3>
        </div>
        <div className='form-floating mb-4'>
          <input
            onChange={(event) => {
              setName(event.target.value)
            }}
            type='name'
            className='form-control'
            placeholder='Car name'
          />
          <label>Enter Company Name</label>
        </div>
        <div className='form-floating mb-4'>
          <input
            onChange={(event) => {
              setModel(event.target.value)
            }}
            type='model'
            className='form-control'
            placeholder='Car Model'
          />
          <label>Enter Car Model</label>
        </div>

        <div className='form-floating mb-4'>
          <input
            onChange={(event) => {
              setPrice(event.target.value)
            }}
            type='number'
            className='form-control'
            placeholder='price'
          />
          <label>Enter Price (in Rs.)</label>
        </div>

        <div className='form-floating mb-4'>
          <input
            onChange={(event) => {
              setCarColor(event.target.value)
            }}
            type='text'
            className='form-control'
            placeholder='Car Color'
          />

          <label>Enter Car Color</label>
        </div>

        <div>
          <button
            onClick={addCar}
            className='btn btn-primary'
            type='submit'
            style={styles.signinButton}>
            <b>Add Car</b>
          </button>
        </div>
      </div>
    </div>
  )
}

const styles = {
  container: {
    width: 500,
    height: 400,
    padding: 25,
    position: 'relative',
    top: 100,
    left: 0,
    right: 0,
    bottom: 0,
    margin: 'auto',
    borderColor: '#db0f62',
    borderRadius: 10,
    borderWidth: 0,
    borderStyle: 'solid',
    boxShadow: '1px 1px 20px 2px #C9C9C9',
  },

  signinButton: {
    position: 'relative',
    width: '100%',
    height: 50,
    backgroundColor: 'gray',
    color: 'white',
    borderRadius: 5,
    border: 'none',
    marginTop: 50,
  },

  background: {
    backgroundImage: `url(${image})`,
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    width: '100vw',
    height: '100vh',
  },
}

export default AddCar
