import { useEffect } from 'react'
import image from '../../images/show.jpg'
import { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import axios from 'axios'
import { toast } from 'react-toastify'
import UploadImage from './uploadImage'

const Mycars = () => {
  const [cars, setCars] = useState([])

  const navigate = useNavigate()

  useEffect(() => {
    //get all cars created by user
    getmyCars()
  }, [])

  const getmyCars = () => {
    axios
      .get('http://localhost:4000/car/myCars', {
        headers: { token: sessionStorage['token'] },
      })
      .then((response) => {
        const result = response.data
        console.log(result)
        if (result['status'] === 'success') {
          setCars(result['data'])
        } else {
          toast.error(result['error'])
        }
      })
  }

  const uploadImage = (id) => {
    navigate('/upload-image', { state: { id: id } })
    console.log(id)
  }

  const deleteCar = (id) => {
    axios
      .delete('http://localhost:4000/car/delete/' + id, {
        headers: { token: sessionStorage.token },
      })
      .then((response) => {
        const result = response.data
        if (result['status'] === 'success') {
          toast.success('Car deleted successfully')
          getmyCars()
        } else {
          toast.error(result['error'])
        }
      })
  }

  return (
    <div style={styles.background}>
      <div className='container' style={{ marginLeft: 1150 }}>
        <h3 style={{ textAlign: 'center', marginBottom: 25, color: 'maroon' }}>
          My Cars
        </h3>
        <table
          className='table table-striped'
          style={{
            textAlign: 'center',
            color: 'Blue',
          }}>
          <thead>
            <tr>
              <th>Id</th>
              <th>Company Name</th>
              <th>Model</th>
              <th>Color</th>
              <th>Price</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {cars.map((car) => {
              return (
                <tr style={{ color: 'Green', fontWeight: 'bold' }}>
                  <td>{car.id}</td>
                  <td>{car.name}</td>
                  <td>{car.model}</td>
                  <td>{car.carColor}</td>
                  <td>{car.price}</td>
                  <td>
                    <button
                      className='btn btn-sm btn-success'
                      style={styles.button}>
                      Edit
                    </button>
                    <button
                      onClick={() => deleteCar(car.id)}
                      className='btn btn-sm btn-danger'
                      style={styles.button}>
                      Delete
                    </button>
                    <button
                      className='btn btn-sm btn-warning'
                      onClick={() => uploadImage(car.id)}>
                      Upload Image
                    </button>
                  </td>
                </tr>
              )
            })}
          </tbody>
        </table>
      </div>
    </div>
  )
}

const styles = {
  h3: {
    textAlign: 'center',
    margin: 20,
  },

  button: {
    marginRight: 10,
  },

  background: {
    backgroundImage: `url(${image})`,
    color: 'black',
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    width: '48vw',
    height: '100vh',
  },
}

export default Mycars
