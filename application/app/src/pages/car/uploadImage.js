import axios from 'axios'
import React from 'react'
import { toast } from 'react-toastify'
import { useLocation } from 'react-router-dom'
import { useNavigate } from 'react-router-dom'
import { useEffect } from 'react'
import { useState } from 'react'
import Button from '../../components/button'
import image from '../../images/home.jpg'

const UploadImage = () => {
  const [id, setId] = useState()
  const [file, setFile] = useState()

  const location = useLocation()

  const navigate = useNavigate()

  useEffect(() => {
    const { id } = location.state
    setId(id)
  }, [])

  const uploadImage = () => {
    //form data is used to send multipart file
    const body = new FormData()
    body.set('photo', file)

    axios
      .post('http://localhost:4000/car/upload-image/' + id, body, {
        headers: {
          'Content-Type': 'multipart/form-data',
          token: sessionStorage['token'],
        },
      })
      .then((response) => {
        const result = response.data
        console.log(result)
        if (result['status'] === 'success') {
          toast.success('image uploaded successfully')
          navigate('/myCars')
        } else if (result['status'] === 'error') {
          toast.error('please add image')
        }
      })
      .catch((error) => {
        console.log('error')
        console.log(error)
      })
  }

  return (
    <div style={styles.background}>
      <div className='container' style={{ fontWeight: 'bold' }}>
        <h3 style={{ textAlign: 'center', color: 'white' }}>Upload Image</h3>
        <div className='mb-3'>
          <label></label>
          <input
            style={{ margin: 20 }}
            onChange={(event) => {
              setFile(event.target.files[0])
            }}
            className='form-control'
            type='file'
          />
          <Button onClick={uploadImage} title='Upload photo'></Button>
        </div>
      </div>
    </div>
  )
}

const styles = {
  background: {
    backgroundImage: `url(${image})`,
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    width: '100vw',
    height: '100vh',
  },
}

export default UploadImage
