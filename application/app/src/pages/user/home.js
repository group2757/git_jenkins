import React from 'react'
import image from '../../images/home.jpg'
import { toast } from 'react-toastify'
import axios from 'axios'
import { useState, useEffect } from 'react'

const Home = () => {
  const [listings, setListings] = useState([])

  // load the homes as soon as the component gets loaded successfully
  useEffect(() => {
    loadHomes()
  }, [])

  const loadHomes = () => {
    axios
      .get('http://localhost:4000/car/', {
        headers: { token: sessionStorage['token'] },
      })
      .then((response) => {
        const result = response.data
        console.log(result)
        if (result['status'] === 'success') {
          setListings(result['data'])
        } else {
          toast.error(result['error'])
        }
      })
  }

  return <div style={styles.background}></div>
}

const styles = {
  background: {
    backgroundImage: `url(${image})`,
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    width: '100vw',
    height: '100vh',
  },
}

export default Home
