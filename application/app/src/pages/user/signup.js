import { Link } from 'react-router-dom'
import image from '../../images/show.webp'
import { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { toast } from 'react-toastify'
import React from 'react'
import axios from 'axios'

const Signup = () => {
  const [companyname, setCompanyname] = useState('')
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [confirmPassword, setConfirmPassword] = useState('')

  const navigate = useNavigate()

  const signup = () => {
    if (companyname.length === 0) {
      toast.error('please enter company name')
    } else if (email.length === 0) {
      toast.error('please enter email')
    } else if (password.length === 0) {
      toast.error('please enter password')
    } else if (confirmPassword.length === 0) {
      toast.error('please enter confirm password')
    } else if (confirmPassword !== password) {
      toast.error('confirm password must be same as password')
    } else {
      axios
        .post('http://localhost:4000/user/signup', {
          companyname,
          email,
          password,
        })
        .then((response) => {
          const result = response.data
          console.log(result)

          if (result['status'] === 'error') {
            toast.error('error')
          } else {
            toast.success('successfully registered new employee')
            navigate('/signin')
          }
        })
    }
  }

  return (
    <div style={styles.background}>
      <div>
        <div style={styles.container}>
          <h3
            style={{
              textAlign: 'center',
              marginBottom: 10,
              color: 'white',
            }}>
            Register Here
          </h3>
          <div className='form-floating mb-3'>
            <input
              onChange={(event) => {
                setCompanyname(event.target.value)
              }}
              type='companyname'
              className='form-control'
              placeholder='Company name'
            />
            <label>Company Name</label>
          </div>
          <div className='form-floating mb-3'>
            <input
              onChange={(event) => {
                setEmail(event.target.value)
              }}
              type='email'
              className='form-control'
              placeholder='Email'
            />
            <label>Enter Email id</label>
          </div>
          <div className='form-floating mb-3'>
            <input
              onChange={(event) => {
                setPassword(event.target.value)
              }}
              type='password'
              className='form-control'
              placeholder='password'
            />
            <label>Enter Password</label>
          </div>

          <div className='form-floating mb-3'>
            <input
              onChange={(event) => {
                setConfirmPassword(event.target.value)
              }}
              type='password'
              className='form-control'
              placeholder='password'
            />
            <label>Confirm Password</label>
          </div>

          <div className='mb-3' style={{ marginTop: 40 }}>
            <div style={{ color: 'white' }}>
              Already have an account?
              <Link to='/signin' style={{ marginLeft: 10 }}>
                <b> Signin here</b>
              </Link>
            </div>
          </div>

          <button
            onClick={signup}
            className='btn btn-primary'
            type='submit'
            style={styles.signinButton}>
            <b> Signup</b>
          </button>
        </div>
      </div>
    </div>
  )
}

const styles = {
  container: {
    width: 400,
    height: 360,
    padding: 25,
    position: 'relative',
    top: 40,
    left: 0,
    right: 0,
    bottom: 0,
    margin: 'auto',
    borderColor: '#db0f62',
    borderRadius: 10,
    borderWidth: 0,
    borderStyle: 'solid',
    boxShadow: '1px 1px 10px 2px #C9C9C9',
  },

  signinButton: {
    position: 'relative',
    width: '100%',
    height: 40,
    backgroundColor: 'gray',
    color: 'white',
    borderRadius: 5,
    border: 'none',
    marginTop: 15,
  },

  background: {
    backgroundImage: `url(${image})`,
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    width: '100vw',
    height: '100vh',
  },
}

export default Signup
