import Input from '../../components/input'
import { Link } from 'react-router-dom'
import image from '../../images/xpeng.jpeg'
import React from 'react'
import axios from 'axios'
import { toast } from 'react-toastify'
import { useState } from 'react'
import { useNavigate } from 'react-router-dom'

const Signin = () => {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')

  const navigate = useNavigate()

  const signin = () => {
    if (email.length === 0) {
      toast.error('please enter email id')
    } else if (password.length === 0) {
      toast.error('please enter password')
    } else {
      axios
        .post('http://localhost:4000/user/signin', {
          email,
          password,
        })
        .then((response) => {
          const result = response.data
          console.log(result)
          if (result['status'] === 'error') {
            toast.error('invalid email or password')
          } else {
            sessionStorage['token'] = result['data']['token']

            toast.success('Welcome to Cars')
            navigate('/myCars')
          }
        })
        .catch((error) => {
          console.log('error')
          console.log(error)
        })
    }
  }

  return (
    <div style={styles.background}>
      <div style={styles.container}>
        <h3
          style={{
            textAlign: 'center',
            marginBottom: 20,
            color: 'white',
          }}>
          Welcome Signin Here
        </h3>
        <div className=' mb-3' style={{ color: 'white' }}>
          <label>
            <b>Email</b>
          </label>
          <Input
            onChange={(event) => {
              setEmail(event.target.value)
            }}
            type='email'
            className='form-control'
          />
        </div>

        <div className='mb-3' style={{ color: 'white' }}>
          <label>
            <b>Password</b>
          </label>
          <Input
            onChange={(event) => {
              setPassword(event.target.value)
            }}
            type='password'
            className='form-control '
          />
        </div>

        <div className='mb-3' style={{ marginTop: 30 }}>
          <div style={{ color: 'white' }}>
            Dont have an account?
            <Link to='/signup' style={{ margin: 10 }}>
              <b>Signup here</b>
            </Link>
          </div>
        </div>

        <button
          onClick={signin}
          className='btn btn-primary'
          type='submit'
          style={styles.signinButton}>
          <b>SignIn</b>
        </button>
      </div>
    </div>
  )
}

const styles = {
  container: {
    width: 425,
    height: 320,
    padding: 25,
    position: 'relative',
    borderRadius: 10,
    top: 100,
    left: -800,
    right: 0,
    bottom: 0,
    margin: 'auto',
    backgroundColor: 'transparent',
    boxShadow: '1px 1px 5px 1px #C9C9C9',
  },

  signinButton: {
    position: 'relative',
    width: '100%',
    height: 40,
    color: 'white',
    borderRadius: 5,
    border: 'none',
    marginTop: 10,
    backgroundColor: '#adb5bd',
  },

  background: {
    backgroundImage: `url(${image})`,
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    width: '100vw',
    height: '100vh',
  },
}

export default Signin
