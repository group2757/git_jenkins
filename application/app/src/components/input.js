const Input = (props) => {
  const { type, title, onChange } = props
  return (
    <div className='mb-3'>
      <label></label>
      <input
        style={{ marginTop: -10, marginBottom: 30 }}
        onChange={onChange}
        type={type ? type : 'text'}
        className='form-control'></input>
    </div>
  )
}
export default Input
