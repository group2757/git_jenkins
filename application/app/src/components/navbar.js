import { Link } from 'react-router-dom'

const Navbar = () => {
  return (
    <nav
      style={{ backgroundColor: 'black' }}
      className='navbar navbar-expand-lg navbar-dark'>
      <div className='container-fluid'>
        <Link className='navbar-brand' to='/home'>
          <h3>Cars</h3>
        </Link>

        <button
          className='navbar-toggler'
          type='button'
          data-bs-toggle='collapse'
          data-bs-target='#navbarSupportedContent'
          aria-controls='navbarSupportedContent'
          aria-expanded='false'
          aria-label='Toggle navigation'>
          <span className='navbar-toggler-icon'></span>
        </button>

        <div className='collapse navbar-collapse'>
          <ul className='navbar-nav me-auto mb-2 mb-lg-0'>
            {/* <li className='nav-item'>
              <Link
                className='nav-link active'
                aria-current='page'
                to='/home'></Link>
            </li> */}
            <li className='nav-item'>
              <Link className='nav-link active' to='/signin'>
                Signin
              </Link>
            </li>

            <li className='nav-item'>
              <Link className='nav-link active' to='/signup'>
                Signup
              </Link>
            </li>

            <li className='nav-item'>
              <Link className='nav-link active' to='/myCars'>
                My Cars
              </Link>
            </li>

            <li className='nav-item'>
              <Link className='nav-link active' to='/addCar'>
                Add Car
              </Link>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Navbar
