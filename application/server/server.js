const express = require('express')

const config = require('./config')
const utils = require('./utils')
const jwt = require('jsonwebtoken')
const cors = require('cors')

const app = express()
app.use(express.json())
app.use(cors())
app.use(express.static('uploads'))

app.use((request, response, next) => {
  if (request.url === '/user/signin' || request.url === '/user/signup') {
    next()
  } else {
    const token = request.headers['token']
    if (!token || token.length === 0) {
      response.send('token is missing')
    } else {
      try {
        const payload = jwt.verify(token, config.secret)
        request.id = payload.id
        next()
      } catch (ex) {
        response.send('inavlid token')
      }
    }
  }
})

const userRouter = require('./routes/user')
const carRouter = require('./routes/car')

app.use('/user', userRouter)
app.use('/car', carRouter)

app.listen(4000, '0.0.0.0', () => {
  console.log('server get started on port 4000')
})
