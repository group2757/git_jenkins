const express = require('express')
const db = require('../db')
const cors = require('cors')
const jwt = require('jsonwebtoken')
const multer = require('multer')

const utils = require('../utils')

const upload = multer({ dest: 'uploads' })

const router = express.Router()

router.post(
  '/upload-image/:id',
  upload.single('photo'),
  (request, response) => {
    const { id } = request.params

    console.log(request.file)

    const filename = request.file.filename

    console.log(filename)

    const statement = `
  Update carTB SET image=?
  WHERE id=?`

    db.pool.query(statement, [filename, id], (error, result) => {
      response.send(utils.createResult(error, result))
    })
  }
)

router.post('/add', (request, response) => {
  const { name, model, price, carColor } = request.body
  const statement = `
        Insert Into carTB (name,model,price, carColor) Values
        (?,?,?,?)`

  db.pool.query(statement, [name, model, price, carColor], (error, result) => {
    response.send(utils.createResult(error, result))
  })
})

router.get('/myCars', (request, response) => {
  const statement = `
          Select * From carTB `

  db.pool.query(statement, (error, result) => {
    response.send(utils.createResult(error, result))
  })
})

router.delete('/delete/:id', (request, response) => {
  const { id } = request.params
  console.log(id)
  const statement = `
            Delete From carTB WHERE id=? `

  db.pool.query(statement, [id], (error, result) => {
    response.send(utils.createResult(error, result))
  })
})

module.exports = router
