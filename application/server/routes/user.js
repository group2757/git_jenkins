const express = require('express')
const db = require('../db')
const utils = require('../utils')
const jwt = require('jsonwebtoken')
const config = require('../config')
const router = express.Router()

router.post('/signin', (request, response) => {
  const { email, password } = request.body
  const statement = `
     select * From userTB Where email=? AND password=?`

  db.pool.query(statement, [email, password], (error, users) => {
    const result = {}
    if (error) {
      result['status'] = 'error'
      result['error'] = error
    } else if (users.length === 0) {
      result['status'] = 'error'
      result['error'] = 'user does not exist'
      console.log(error)
    } else {
      const user = users[0]
      const payload = { id: user['id'] }
      const token = jwt.sign(payload, config.secret)
      result['status'] = 'success'
      result['data'] = {
        name: user['name'],
        email: user['email'],
        id: user['id'],
        token,
      }
    }
    response.send(result)
  })
})

router.post('/signup', (request, response) => {
  const { companyname, email, password } = request.body
  console.log(request.body)
  const statement = `
      Insert into userTB (companyname, email, password) values (?,?,?)`

  db.pool.query(statement, [companyname, email, password], (error, result) => {
    response.send(utils.createResult(error, result))
  })
})

module.exports = router
